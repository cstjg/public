package net.garagefarm.rhinterface.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;

import net.garagefarm.lib.entities.BlenderSettings;
import net.garagefarm.lib.entities.Job;
import net.garagefarm.lib.entities.JobSettings;
import net.garagefarm.lib.entities.LWSettings;
import net.garagefarm.lib.entities.MaxSettings;
import net.garagefarm.lib.entities.MaxwellSettings;
import net.garagefarm.lib.entities.MayaSettings;
import net.garagefarm.lib.entities.ModoSettings;
import net.garagefarm.lib.entities.SubJob;
import net.garagefarm.lib.types.JobStates;
import net.garagefarm.parser.modo.ModoParser;
import net.garagefarm.rhinterface.InterfaceMain;
import net.garagefarm.rhinterface.connection.SendMessages;
import net.garagefarm.rhinterface.controller.Controller;
import net.garagefarm.rhinterface.gui.job.BlenderJobWindow;
import net.garagefarm.rhinterface.gui.job.LwJobWindow;
import net.garagefarm.rhinterface.gui.job.MaxJobWindow;
import net.garagefarm.rhinterface.gui.job.MaxwellJobWindow;
import net.garagefarm.rhinterface.gui.job.MayaJobWindow;
import net.garagefarm.util.ClipboardUtils;
import net.garagefarm.util.Popups;
import net.garagefarm.util.WinUtils;

public class JobPopup extends CustomPopup {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String sep = System.getProperty("file.separator");
	private final Controller controller = InterfaceMain.getController();
	private JMenuItem view;
	private JMenuItem edit;
	private JMenuItem check;
	private JMenuItem clone;
	private JMenuItem openDir;
	private JMenuItem command;
	private JMenuItem kill;
	private JMenuItem pause;
	private JMenuItem resume;
	private JMenuItem archive;

	public JobPopup() {
		super();
		setMenu();
		initListeners();
	}

	private  void setMenu() {
		removeAll();

		view = new JMenuItem("view");
		add(view);

		edit = new JMenuItem("edit");
		add(edit);

		check = new JMenuItem("check");
		add(check);
		clone = new JMenuItem("clone");
		add(clone);
		openDir = new JMenuItem("open dir");
		add(openDir);
		command = new JMenuItem("command");
		add(command);

		kill = new JMenuItem("kill");
		add(kill);
		pause = new JMenuItem("pause");
		add(pause);
		resume = new JMenuItem("resume");
		add(resume);
		archive = new JMenuItem("archive");
		add(archive);
	}

	private void initListeners() {

		view.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Job job = controller.getJobs().get(rows[0]);
				MainPanel.getTabs().setSelectedIndex(2);
				MainPanel.getSubJobStatsPanel().getJobList().setSelectedValue(job.getName(), true);
				Integer id = job.getSettings().getId();
				controller.getSubJobStatsTableModel().setSubJobs(new ArrayList<SubJob>());
				SendMessages.getSubJobs(id);
			}
		});

		edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Job job = controller.getJobs().get(rows[0]);
				JobSettings set = job.getSettings();

				int t = set.getType();

				if (t >= 0 && t < 20) {
					new MaxJobWindow(job);
				} else if (t >= 20 && t < 40) {
					new MayaJobWindow(job);
				} else if (t >= 40 && t < 60) {
					new ModoParser(job);
				}

				else if (t >= 200 && t < 300) {
					BlenderSettings bSet = (BlenderSettings) set;
					new BlenderJobWindow(bSet);
				} else if (t >= 300 && t < 400) {
					new MaxwellJobWindow(job);
				}

			}
		});

		check.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> names = new ArrayList<>();
				for (int i : rows) {
					Job job = controller.getJobs().get(i);
					if (job.getState() == JobStates.ACTIVE) {
						Popups.info("Checking an active job? really?");
					} else if (job.getState() == JobStates.KILLED) {
						Popups.info("It's dead for sure, don`t check");
					} else {
						names.add(job.getName());
					}
				}
				SendMessages.checkJob(names);
			}
		});

		clone.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JobSettings set = controller.getJobs().get(rows[0]).getSettings();
				int t = set.getType();
				if (t >= 0 && t < 20) {
					MaxSettings maxSet = (MaxSettings) set;
					new MaxJobWindow(maxSet);
				} else if (t >= 20 && t < 40) {
					MayaSettings maySet = (MayaSettings) set;
					new MayaJobWindow(maySet);
				} else if (t >= 40 && t < 60) {
					ModoSettings modSet = (ModoSettings) set;
					new ModoParser(modSet, true);
				} else if (t >= 100 && t < 200) {
					LWSettings lwset = (LWSettings) set;
					new LwJobWindow(lwset);
				} else if (t >= 200 && t < 300) {
					BlenderSettings bSet = (BlenderSettings) set;
					new BlenderJobWindow(bSet);
				} else if (t >= 300 && t < 400) {
					MaxwellSettings bSet = (MaxwellSettings) set;
					new MaxwellJobWindow(bSet);
				}

			}
		});

		openDir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String path = controller.getJobs().get(rows[0]).getSettings().getOutFilePath();
				WinUtils.openBrowser(path);

			}
		});

		command.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				MaxSettings set = (MaxSettings) controller.getJobs().get(rows[0]).getSettings();
				String exe = set.getExePath();
				String in = set.getInFilePath();
				int width = set.getWidth();
				int height = set.getHeight();
				double aspect = set.getAspect();
				boolean rfw = set.isRenderFrameWindow();

				String outFile = set.getOutFilePath() + sep + set.getOutFileName() + '.' + set.getOutFileExt();

				String cmd = exe;
				cmd += " \"" + in + "\"";
				cmd += " -width:" + width;
				cmd += " -height:" + height;
				cmd += " -pixelAspect:" + aspect;
				cmd += " -bitmapPath:\"" + in.substring(0, in.lastIndexOf(sep)) + "\"";
				cmd += " -rfw:" + rfw;
				if (set.isContinueOnError()) {
					cmd += " -continueOnError ";
				}
				cmd += " " + set.getCustomSwitch() + " ";
				cmd += " -outputName:\"" + outFile + "\"";

				ClipboardUtils cu = new ClipboardUtils();
				cu.write(cmd);

			}
		});

		kill.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> names = new ArrayList<>();
				for (int i : rows) {
					Job job = controller.getJobs().get(i);
					if (job.getState() == JobStates.KILLED) {
						Popups.info("don't kill the dead");
					} else {
						names.add(job.getName());

					}
				}
				SendMessages.killJobs(names);

			}
		});

		resume.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> names = new ArrayList<>();
				for (int i : rows) {
					names.add(controller.getJobs().get(i).getName());
				}
				SendMessages.resumeJobs(names);

			}
		});

		pause.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> names = new ArrayList<>();
				for (int i : rows) {
					names.add(controller.getJobs().get(i).getName());
				}
				SendMessages.pauseJobs(names);

			}
		});

		archive.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> names = new ArrayList<>();
				for (int i : rows) {
					Job job = controller.getJobs().get(i);
					if ((job.getState() == JobStates.ACTIVE || job.getState() == JobStates.PAUSED) && job.getRunningCount()>0) {
						Popups.info("don't archive - subjobs may still be on nodes, try killing first");
					} else {
						names.add(controller.getJobs().get(i).getName());
					}
				}
				SendMessages.archiveJobs(names);
			}
		});

	}
}
